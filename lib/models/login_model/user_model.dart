class UserData{
  String? token;
  User? user;

  UserData({this.token, this.user});

  UserData.fromJson(Map<String, dynamic> json) {
    token = json['token'];
    user = json['user'] != null ? new User.fromJson(json['user']) : null;
  }

}
class User {
  String? id;
  String? username;
  String? email;
  String? firstName;
  String? lastName;
  String? phone;
  int? company;
  List<Roles?>? roles;
  List<dynamic>? warehouses;
  Workday? workday;
  bool? isStaff;
  String? lang;
  bool? hasMultipleCompanies;
  bool? termsAndConditionsAccepted;

  User(
      {this.id,
        this.username,
        this.email,
        this.firstName,
        this.lastName,
        this.phone,
        this.company,
        this.roles,
        this.warehouses,
        this.workday,
        this.isStaff,
        this.lang,
        this.hasMultipleCompanies,
        this.termsAndConditionsAccepted});

  User.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    username = json['username'];
    email = json['email'];
    firstName = json['first_name'];
    lastName = json['last_name'];
    phone = json['phone'];
    company = json['company'];
    if (json['roles'] != null) {
      roles = <Roles>[];
      json['roles'].forEach((v) {
        roles!.add(new Roles.fromJson(v));
      });
    }
    // if (json['warehouses'] != null) {
    //   warehouses =  <dynamic>[];
    //   json['warehouses'].forEach((v) {
    //     warehouses!.add(new d.fromJson(v));
    //   });
    // }
    workday =
    json['workday'] != null ? new Workday.fromJson(json['workday']) : null;
    isStaff = json['is_staff'];
    lang = json['lang'];
    hasMultipleCompanies = json['has_multiple_companies'];
    termsAndConditionsAccepted = json['terms_and_conditions_accepted'];
  }

}

class Roles {
  int? id;
  String? name;
  String? description;
  dynamic company;
  List<Permissions>? permissions;

  Roles({this.id, this.name, this.description, this.company, this.permissions});

  Roles.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    description = json['description'];
    company = json['company'];
    if (json['permissions'] != null) {
      permissions = <Permissions>[];
      json['permissions'].forEach((v) {
        permissions!.add(new Permissions.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['description'] = this.description;
    data['company'] = this.company;
    if (this.permissions != null) {
      data['permissions'] = this.permissions!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Permissions {
  String? code;

  Permissions({this.code});

  Permissions.fromJson(Map<String, dynamic> json) {
    code = json['code'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['code'] = this.code;
    return data;
  }
}

class Workday {
  dynamic user;
  dynamic date;
  dynamic startTime;
  dynamic endTime;
  int? workTime;
  int? pauseTime;
  int? status;
  List<dynamic>? activities;
  dynamic startNotes;
  dynamic endNotes;

  Workday(
      {this.user,
        this.date,
        this.startTime,
        this.endTime,
        this.workTime,
        this.pauseTime,
        this.status,
        this.activities,
        this.startNotes,
        this.endNotes});

  Workday.fromJson(Map<String, dynamic> json) {
    user = json['user'];
    date = json['date'];
    startTime = json['start_time'];
    endTime = json['end_time'];
    workTime = json['work_time'];
    pauseTime = json['pause_time'];
    status = json['status'];
    startNotes = json['start_notes'];
    endNotes = json['end_notes'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['user'] = this.user;
    data['date'] = this.date;
    data['start_time'] = this.startTime;
    data['end_time'] = this.endTime;
    data['work_time'] = this.workTime;
    data['pause_time'] = this.pauseTime;
    data['status'] = this.status;
    if (this.activities != null) {
      data['activities'] = this.activities!.map((v) => v.toJson()).toList();
    }
    data['start_notes'] = this.startNotes;
    data['end_notes'] = this.endNotes;
    return data;
  }
}