import 'package:conditional_builder_null_safety/conditional_builder_null_safety.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:stock_agile/ui/screens/login/cubit/cubit.dart';
import 'package:stock_agile/ui/screens/login/cubit/states.dart';
import 'package:stock_agile/utils/component/button_text.dart';
import 'package:stock_agile/utils/component/text_custom.dart';
import 'package:stock_agile/utils/component/text_field_custom.dart';
import 'package:stock_agile/utils/config_size/size_config.dart';
import 'package:stock_agile/utils/constants/colors.dart';
import 'package:stock_agile/utils/constants/dialogs.dart';
import 'package:stock_agile/utils/constants/theme_text.dart';

class LoginView extends StatelessWidget {
  final globalKey = GlobalKey<FormState>();

  final TextEditingController textUserName = TextEditingController();
  final TextEditingController textPassWord = TextEditingController();

  @override
  Widget build(BuildContext context) {
    SizedConf().init(context);
    final h = MediaQuery.of(context).size.height;
    final w = MediaQuery.of(context).size.width;

    return BlocProvider(
      create: (context) => LoginCubit(),
      child: BlocConsumer<LoginCubit, LoginStates>(
        listener: (context, state) {},
        builder: (context, state) {
          return Scaffold(
              body: SafeArea(
            child: Container(
              height: h,
              width: w,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Column(
                    children: [
                      Dialogs.sizedBoxH(85),
                      Image.asset(
                        "assets/icons/icon.png",
                      ),
                      Dialogs.sizedBoxH(15),
                      TextCustom(
                        text: "Iniciar sesión.",
                        textStyle: textStyle24,
                      ),
                      Dialogs.sizedBoxH(8),
                      TextCustom(
                        text: "Bienvenid@ de nuevo",
                        textStyle: textStyle14,
                      ),
                      Dialogs.sizedBoxH(50),
                    ],
                  ),
                  Form(
                    key: globalKey,
                    child: Column(
                      children: [
                        TextFieldCustom(
                          labelText: 'User Name',
                          textInputAction: TextInputAction.done,
                          obscureText: false,
                          borderColor: textUserName.text.isEmpty?kBlackColor:kBlueColor,
                          type: TextInputType.emailAddress,
                          textEditingController: textUserName,
                          validator: (value) {
                            if (value!.isEmpty) return 'Please Enter User Name';
                          },
                          widget: SizedBox.shrink(),
                        ),
                        Dialogs.sizedBoxH(25),
                        TextFieldCustom(
                          labelText: 'Password',
                          type: TextInputType.visiblePassword,
                          textInputAction: TextInputAction.done,
                          borderColor: textPassWord.text.isEmpty?kBlackColor:kBlueColor,
                          textEditingController: textPassWord,
                          obscureText:LoginCubit.get(context).isVisible,
                          validator: (value) {
                            if (value!.isEmpty) return 'Please Enter passWord';
                          },
                          widget: IconButton(
                            icon: Icon(
                              LoginCubit.get(context).suffix,
                              color:!LoginCubit.get(context).isVisible?kRedColor: kBlackColor,
                            ),
                            onPressed: () {
                              LoginCubit.get(context).changeIcon();
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                  Column(
                    children: [
                      TextCustom(
                        text: 'I forgot my password',
                        textStyle: textStyle14.copyWith(color: kBlueColor),
                      ),
                      Dialogs.sizedBoxH(15),
                      ConditionalBuilder(
                          condition: state is! LoginLoadStates,
                          builder: (context) =>TextBtn(
                            width: w-100,
                            text: 'Login',
                            onPressed: () async{
                              if(globalKey.currentState!.validate()){
                               await LoginCubit.get(context).userLogin(context: context,userName: textUserName.text.toString(),
                                    passWord: textPassWord.text.toString());
                              }
                            },
                          ),
                          fallback: (context) => CircularProgressIndicator(
                                strokeWidth: 0.8,
                                color: kRedColor,
                              ))
                    ],
                  ),
                ],
              ),
            ),
          ));
        },
      ),
    );
  }
}
