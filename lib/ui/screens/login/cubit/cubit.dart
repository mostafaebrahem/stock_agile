import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:stock_agile/models/login_model/user_model.dart';
import 'package:stock_agile/ui/screens/home_page/home_view.dart';
import 'package:stock_agile/ui/screens/login/cubit/states.dart';
import 'package:stock_agile/utils/config_size/size_config.dart';
import 'package:stock_agile/utils/constants/colors.dart';
import 'package:stock_agile/utils/constants/dialogs.dart';
import 'package:stock_agile/utils/shared/network/dio_helper.dart';
import 'package:stock_agile/utils/shared/network/endpoint.dart';

class LoginCubit extends Cubit<LoginStates>{

  IconData suffix = Icons.visibility_outlined;
  bool isVisible = true;
  UserData? _userData ;

  LoginCubit() : super(LoginInitialStates());

 static LoginCubit get(context) =>BlocProvider.of(context);

//jordibruna For Login In TEST

  Future<void> userLogin({context,required String userName ,required String passWord})async{
   emit(LoginLoadStates());

   try{

   await DioNetwork().postData(url: login_Url, data: {
    "username": userName,
    "password": passWord
  }).then((value)async{
     if(value.statusCode==200)
     {
       await ScaffoldMessenger.of(context).showSnackBar(Dialogs.snackBar(
           content: 'Logged in successfully',
           colorBackGround: kGreyColor,
           marginH: MediaQuery.of(context).size.height-getProportionateScreenWidth(200.0),
           l: getProportionateScreenWidth(15.0)
       ));
       _userData=UserData.fromJson(value.data);

       Navigator.push(context, MaterialPageRoute(builder: (context) => HomePageView(userData: _userData,),));
       print(_userData!.user!.firstName);
       emit(LoginSuccessStates());
     }
   });
   }on DioError   catch(onError){
     if (onError.response?.statusCode==400)
     {
       ScaffoldMessenger.of(context).showSnackBar(Dialogs.snackBar(
           content: onError.response?.data?['non_field_errors'][0],
           colorBackGround: kRedColor ,
           marginH: MediaQuery.of(context).size.height-getProportionateScreenWidth(200.0),
           l: getProportionateScreenWidth(15.0)
       ));
       emit(LoginErrorStates(error:onError.response?.data?.toString()));

     }
   }




 }


  void changeIcon(){
    isVisible = ! isVisible;
    suffix = isVisible?Icons.visibility_outlined:Icons.visibility_off_outlined ;
    emit(LoginVisiblePassWordStates());
  }

}