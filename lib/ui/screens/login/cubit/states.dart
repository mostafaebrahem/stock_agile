abstract class LoginStates {}

class LoginInitialStates extends LoginStates{}
class LoginLoadStates extends LoginStates{}
class LoginSuccessStates extends LoginStates{}
class LoginErrorStates extends LoginStates{
  final String? error;
  LoginErrorStates({this.error});
}

class LoginVisiblePassWordStates extends LoginStates{}
