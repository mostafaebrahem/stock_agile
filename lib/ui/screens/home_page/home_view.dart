import 'package:flutter/material.dart';
import 'package:stock_agile/models/login_model/user_model.dart';
import 'package:stock_agile/utils/component/text_custom.dart';
import 'package:stock_agile/utils/constants/colors.dart';
import 'package:stock_agile/utils/constants/theme_text.dart';

class HomePageView extends StatelessWidget {
  final  UserData? userData;

  HomePageView({this.userData});
  @override
  Widget build(BuildContext context) {

    final h = MediaQuery.of(context).size.height;
    final w = MediaQuery.of(context).size.width;
    return Scaffold(
      backgroundColor: kWhiteColor,
      body: Container(
        height: h,
        width: w,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            TextCustom(
              text:'Welcome',
              textStyle: textStyle24,
            ),
            TextCustom(
              text: userData!.user!.firstName,
              textStyle: textStyle24,
            ),
          ],
        ),
      ),
    );
  }
}
