import 'package:flutter/material.dart';
import 'package:stock_agile/utils/config_size/size_config.dart';
import 'package:stock_agile/utils/constants/colors.dart';

class TextFieldCustom extends StatelessWidget {

  final String? labelText;
  final TextEditingController? textEditingController ;
  final TextInputAction? textInputAction;
  final Color? borderColor ;
  final bool? obscureText ;
  final TextInputType? type;
  final String? Function(String?)? validator;
  final Widget? widget;

  TextFieldCustom({this.textEditingController,this.labelText,this.textInputAction,
    this.obscureText,
    this.validator,
    this.type,
    this.borderColor=kBlackColor,this.widget});

  @override
  Widget build(BuildContext context) {
    final w= MediaQuery.of(context).size.width;
    return  Container(
      width: w-100,
      child: TextFormField(
        controller: textEditingController,
        textInputAction: textInputAction,
        obscureText: obscureText!,
        validator:validator,
        keyboardType: type,
        style: TextStyle(
          decoration: TextDecoration.none
        ),
        decoration:  InputDecoration(
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(color:kBlueColor, width: 1.0),
          ),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(color:borderColor!, width: 1.0),
          ),

          errorBorder: OutlineInputBorder(
            borderSide: BorderSide(color:kRedColor, width: 1.0),
          ),
          border:  OutlineInputBorder(
              borderRadius: BorderRadius.circular(getProportionateScreenWidth(10.0)),
              borderSide: new BorderSide(color:kBlackColor)),
          contentPadding: EdgeInsets.only(top: getProportionateScreenWidth(15.0),
              left: getProportionateScreenWidth(15.0)),
          labelText: labelText,
          suffixIcon:widget!,
          labelStyle: TextStyle(
            color: borderColor
          ),
      ),
    ));
  }
}
