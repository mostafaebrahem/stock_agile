import 'package:flutter/material.dart';
import 'package:stock_agile/utils/config_size/size_config.dart';

class TextCustom extends StatelessWidget {
  final String? text;
  final TextStyle? textStyle;

   TextCustom({this.text,this.textStyle});

  @override
  Widget build(BuildContext context) {
    return Text(text!,
    style: textStyle,
    );
  }
}
