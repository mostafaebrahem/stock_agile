import 'package:flutter/material.dart';
import 'package:stock_agile/utils/component/text_custom.dart';
import 'package:stock_agile/utils/config_size/size_config.dart';
import 'package:stock_agile/utils/constants/colors.dart';
import 'package:stock_agile/utils/constants/theme_text.dart';

class TextBtn extends StatelessWidget {

  final double? width ;
  final Function()? onPressed;
  final String? text;

  TextBtn({this.text,this.onPressed,this.width});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      decoration: BoxDecoration(
          color: kBlueColor,
          borderRadius: BorderRadius.circular(
              getProportionateScreenWidth(10.0))),
      child: TextButton(
        onPressed:onPressed,
        child: TextCustom(
          text: text,
          textStyle: textStyle14.copyWith(
              color: kWhiteColor),
        ),
      ),
    );
  }
}
