import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:stock_agile/utils/config_size/size_config.dart';
import 'package:stock_agile/utils/constants/colors.dart';

final textStyle24 = GoogleFonts.openSans(fontSize: getProportionateScreenWidth(24),
  color:kBlackColor,
  fontWeight: FontWeight.w700,
  fontStyle: FontStyle.normal,
);

final textStyle14 = GoogleFonts.openSans(fontSize: getProportionateScreenWidth(14),
    color:kGreyColor,
    fontWeight: FontWeight.w400,
    fontStyle: FontStyle.normal,
);