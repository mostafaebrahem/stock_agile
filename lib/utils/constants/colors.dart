import 'package:flutter/material.dart';

const kWhiteColor = Colors.white;
const kBlueColor = Colors.blue;
const kBlackColor = Color(0xff010000);
const kGreyColor = Color(0xff4D4D4D);
const kRedColor = Color(0xffD53300);