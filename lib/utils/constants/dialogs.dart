import 'package:flutter/material.dart';
import 'package:stock_agile/utils/component/text_custom.dart';
import 'package:stock_agile/utils/config_size/size_config.dart';
import 'package:stock_agile/utils/constants/colors.dart';
import 'package:stock_agile/utils/constants/theme_text.dart';

class  Dialogs{


  static snackBar({String? content,double? w ,Color? colorBackGround = kBlueColor,double? marginH,double? l}){
    return SnackBar(content:TextCustom(
      text:content ,
      textStyle: textStyle14.copyWith(color: kWhiteColor),
    ),
      // width: w,
      backgroundColor: colorBackGround,
      behavior: SnackBarBehavior.floating
      ,margin:EdgeInsets.only(bottom: getProportionateScreenWidth(marginH!),
      left: getProportionateScreenWidth(l!),
        right: getProportionateScreenWidth(l)
      ),duration: Duration(milliseconds: 800),);
  }

  static sizedBoxH(double? h){
    return SizedBox(height: getProportionateScreenWidth(h!),);
  }


 }