import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:stock_agile/utils/shared/network/endpoint.dart';

abstract class DioHelper {
  Future<Response> postData({
    @required String url,
    @required dynamic data,
    String token,
  });

  Future<Response> putData({
    @required String url,
    @required dynamic data,
    String token,
  });

  Future<Response> getData({
    @required String url,
    dynamic query,
    String token,
  });
  Future<Response> deleteData({
    @required String url,
    String token,
  });
}

class DioNetwork extends DioHelper {
  final Dio _dio = Dio(
    BaseOptions(
      baseUrl: baseUrl,
      receiveDataWhenStatusError: true,
      followRedirects: false,
      headers: {
        'Accept': 'application/json',
      },
    ),
  );
  @override
  Future<Response> getData({String? url, dynamic query, String? token}) async {
    _dio.options.headers = {
      'Authorization': token ?? '',
    };
    return await _dio.get(
      url!,
      queryParameters: query,
    );
  }

  @override
  Future<Response> postData({String? url, data, String? token}) async {
    _dio.options.headers = {
      'Authorization': token ?? '',
    };
    return await _dio.post(
      url!,
      data: data,
    );
  }

  @override
  Future<Response> putData({String? url, data, String? token}) async {
    _dio.options.headers = {
      'Authorization': token ?? '',
    };

    return await _dio.put(
      url!,
      data: data,
    );
  }

  @override
  Future<Response> deleteData({String? url, String? token}) async {
    _dio.options.headers = {
      'Authorization': token ?? '',
    };

    return await _dio.delete(url!);
  }
}
